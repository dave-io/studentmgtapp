import psycopg2
from models import ClassModel
from models import Students
from database import CursorFromConnectionPool, Database

Database.initialise(user='postgres', password='Grac10u50++', database='student_mgt_db', host='localhost')


def menu():
    """This function encapsulates major operations the user wants to perform"""
    print("Welcome to your student management application!")
    print("Press 1 to manage class")
    print("Press 2 to manage students")
    print("Press 3 to add codename to a classroom or student")
    print("Press 4 to add a new field to classroom or student")
    option = int(input("***************************\n"))
    if option == 1:
        manage_class()
    elif option == 2:
        manage_students()
    elif option == 3:
        add_code_to_table()
    elif option == 4:
        add_column_to_table(table_name=None, column_name=None)


def check_if_classroom_exists(class_name):
    """With an SQL query, check existence of a classroom in the database"""
    with CursorFromConnectionPool() as cursor:
        cursor.execute("SELECT class_id FROM class_room WHERE class_name = %s;", (class_name,))
        row = cursor.fetchone()
        if not row:
            print("Not found!!")
            return False
        else:
            return row


def check_if_student_exists(first_name, last_name):
    """With an SQL query, check existence of a student in the database"""
    with CursorFromConnectionPool() as cursor:
        cursor.execute("SELECT class_id FROM students WHERE first_name = %s AND last_name = %s;",
                       (first_name, last_name))
        row = cursor.fetchone()
        if not row:
            print("Not found!")
            return False
        else:
            return row


def manage_class():
    """Here's the user's go-to section to manage operations around classrooms"""
    print("Press 1 to CREATE class")
    print("Press 2 to UPDATE class")
    option = int(input("************************\n"))
    if option == 1:
        create_class()
    elif option == 2:
        update_class()


def create_class():
    """User creates a new classroom if it doesn't currently exists"""
    status = True
    while status:
        class_name = input("Enter a class name: ")
        if check_if_classroom_exists(class_name):
            print("A classroom with name {} currently exists!!".format(class_name))
            continue
        else:
            print("It's being created now\n")
            class_size = int(input("What's your class size? "))
            class_floor = input("On what floor is your class? ")
            teacher = input("Class teacher's name: ")
            age_bracket = int(input("Enter students' average age: "))

            new_class = ClassModel(class_name, class_size, class_floor, teacher, age_bracket).save_class_to_db()

        print("Class created successfully!!!\n")
        choice = input("Do you want to create another class? [Y = Yes, N = No]\n")
        if choice == 'Y':
            continue
        elif choice == 'N':
            status = False


def get_classroom(class_id=None):
    """This method fetches and returns a list of existing classrooms for the user to see"""

    class_payload = []
    if class_id:
        """If class_id is provided, fetch and return the classroom by its Id"""
        with CursorFromConnectionPool() as cursor:
            cursor.execute("SELECT * FROM class_room WHERE class_id = %s", (class_id,))
            classroom_data = cursor.fetchone()
            class_payload.append(dict(class_name=classroom_data[1], teacher=classroom_data[4],
                                      class_id=classroom_data[0]))
            return class_payload
    else:
        """If class_id is not provided, fetch and return all classrooms"""
        with CursorFromConnectionPool() as cursor:
            cursor.execute("SELECT * FROM class_room;")
            classroom_data = cursor.fetchall()
            print(classroom_data[0])
            for row in classroom_data:
                print(row[2])
                classroom = {
                    "class_name": row[1],
                    "teacher": row[4],
                    "class_id": row[0]
                }
                class_payload.append(classroom)
            return class_payload


def update_class():
    """User updates a class after a check confirms it exists"""
    status = True
    while status:
        class_room = get_classroom()
        print("classroom(s) ", class_room)
        class_name = input("Enter the name of class to be updated: ")
        if not check_if_classroom_exists(class_name):
            print("No classroom with the name {} exists on the system.".format(class_name))
            continue
        else:
            class_size = int(input("What's the class size? "))
            class_floor = input("On what floor is your class? ")
            teacher = input("Class teacher's name: ")
            age_bracket = int(input("Enter students' average age: "))

            new_class = ClassModel(class_name, class_size, class_floor, teacher, age_bracket).update_class_to_db()

            print("Class updated successfully!!!\n")
            print("updated class ", get_classroom(2))
            choice = input("Do you want to update another class? [Y = Yes, N = No]\n")
            if choice == 'Y':
                continue
            elif choice == 'N':
                status = False


def manage_students():
    """Here's the user's go-to section to manage operations around students"""
    print("Press 1 to ADD a student to a class")
    print("Press 2 to UPDATE student's record")
    print("Press 3 to DELETE student's record")
    print("Press 4 to CHANGE student's class")
    option = int(input("***************************\n"))
    if option == 1:
        add_student()
    elif option == 2:
        update_student_record()
    elif option == 3:
        delete_student_record()
    elif option == 4:
        move_student()


def add_student():
    """User adds a new student to a classroom if they don't currently exist"""
    status = True
    while status:
        class_name = input("Enter the name of class to add student to: ")
        first_name = input("Enter student's first name: ")
        last_name = input("Enter student's last name: ")
        student_check = check_if_student_exists(first_name, last_name)
        with CursorFromConnectionPool() as cursor:
            cursor.execute("SELECT class_id FROM class_room WHERE class_name = %s;", (class_name,))
            row = cursor.fetchone()
            """Check existence of student and non-existence of classroom
            to prevent duplicates and program failure"""
            if not row and student_check is True:
                print("Either {} doesn't exist or the name {} {} already exists!!".format(class_name,
                                                                                          first_name,
                                                                                          last_name))
                continue
            else:
                print("Student is being added now\n")
                # print(student_check())
                age = int(input("Enter student's age: "))
                gender = input("Enter student's gender: ")
                dob = input("Enter student's date of birth: ")
                reg_date = input("Enter date of registration: ")
                phone = input("Enter parent's phone number: ")
                class_id = row[0]

            new_student = Students(first_name, last_name, age, gender, dob, reg_date, phone,
                                   class_id).save_student_to_db()

            print("Student added successfully!!!\n")
            choice = input("Do you want to add another student? [Y = Yes, N = No]\n")
            if choice == 'Y':
                continue
            elif choice == 'N':
                status = False


def get_students(student_id=None):
    """Read student data from the database, by ID or without ID"""
    if student_id:
        with CursorFromConnectionPool() as cursor:
            cursor.execute("SELECT * FROM students WHERE student_id = %s", (student_id,))
            student_data = cursor.fetchone()
            return dict(student_id=student_data[0], first_name=student_data[1], last_name=student_data[2],
                        class_id=student_data[8])
    else:
        with CursorFromConnectionPool() as cursor:
            cursor.execute("SELECT * FROM students;")
            student_data = cursor.fetchall()
            student_payload = []  # initialise empty list into which to append student_data
            for row in student_data:
                student = {
                    "student_id": row[0],
                    "first_name": row[1],
                    "last_name": row[2],
                    "class_id": row[8]
                }
                student_payload.append(student)
            return student_payload


def update_student_record():
    """User updates student's record(s) upon check that they exist"""
    all_classrooms = get_classroom()
    if all_classrooms is dict:
        # one record
        print("Name {}, ID {}".format(all_classrooms["class_name"], all_classrooms["class_id"]))
    else:
        # many records
        for classroom in all_classrooms:
            print("Name {}, ID {}".format(classroom["class_name"], classroom["class_id"]))

    status = True
    while status:
        print("Select the ID for the classroom from the list above\n")
        class_id = int(input("Classroom id: "))
        first_name = input("Enter student's first name: ")
        last_name = input("Enter student's last name: ")
        student_check = check_if_student_exists(first_name, last_name)

        with CursorFromConnectionPool() as cursor:
            cursor.execute("SELECT class_id FROM class_room WHERE class_id = %s;", (class_id,))
            class_record = cursor.fetchone()
            """Check existence of student and non-existence of classroom
            to prevent duplicates and program failure"""

            if not class_record and student_check:
                print("Couldn't match {} {} to the class ID provided!!".format(first_name, last_name))
                continue
            else:
                print("Student's record is being updated now\n")
                age = int(input("Enter student's age: "))
                gender = input("Enter student's gender: ")
                dob = input("Enter student's date of birth: ")
                reg_date = input("Enter date of registration: ")
                phone = input("Enter parent's phone number: ")

            new_student = Students(first_name, last_name, age, gender, dob, reg_date, phone,
                                   class_id).update_student_to_db()

            print("Student's record updated successfully!!!\n")
            print("updated record: ", get_students())

            choice = input("Do you want to update another student's record? [Y = Yes, N = No]\n")
            if choice == 'Y':
                continue
            elif choice == 'N':
                status = False


def add_column_to_table(table_name, column_name):
    """Add extra column to either classroom or students table with no data"""
    print("Type 'class_room' to add column to class category")
    print("Type 'students' to add column to students category")
    option = input("************************\n")
    if option == 'class_room':
        column_name = input("Enter column name: ")
        table_name = 'class_room'
        with CursorFromConnectionPool() as cursor:
            sql = "ALTER TABLE %s ADD COLUMN %s VARCHAR(10) UNIQUE;" % (table_name, column_name)
            print(sql)
            cursor.execute(sql)
    elif option == 'students':
        column_name = input("Enter column name: ")
        table_name = 'students'
        with CursorFromConnectionPool() as cursor:
            sql = "ALTER TABLE %s ADD COLUMN %s VARCHAR(10) UNIQUE;" % (table_name, column_name)
            print(sql)
            cursor.execute(sql)


def add_code_to_table():
    """Add codename to a class (e.g ENG212) or student for unique identification"""
    status = True
    while status:
        print("Press 1 to add class code")
        print("Press 2 to add student code")
        option = int(input("************************\n"))
        if option == 1:
            """Go the classroom route"""
            class_name = input("Enter the name of class to add code to: ")
            if not check_if_classroom_exists(class_name):
                print("No classroom with the name {} exists on the system.".format(class_name))
                continue
            else:
                class_code = input("Enter your code here: ")
                with CursorFromConnectionPool() as cursor:
                    cursor.execute("UPDATE class_room SET class_code = %s WHERE class_name = %s;",
                                   (class_code, class_name))
                    print("Code added successfully!")
                    status = False

        elif option == 2:
            """Go the students route"""
            student = get_students()
            print("student(s) ", student)
            first_name = input("Enter first name of student to add code to: ")
            last_name = input("Enter student's last name: ")
            if not check_if_student_exists(first_name, last_name):
                print("No student with the name {} {} exists on the system.".format(first_name, last_name))
                continue
            else:
                student_code = input("Enter your code here: ")
                with CursorFromConnectionPool() as cursor:
                    cursor.execute("UPDATE students SET student_code = %s WHERE first_name = %s AND last_name = %s;",
                                   (student_code, first_name, last_name))
                    print("Code added successfully!")
                    status = False


def move_student(first_name):
    """Move a student from one class to another by class_id"""
    class_id = int(input("Enter student's new class id here: "))
    with CursorFromConnectionPool() as cursor:
        try:
            cursor.execute("""ALTER TABLE students 
                            DROP CONSTRAINT class_id_fkey""")
        except (Exception, psycopg2.Error) as error:
            print("Error in operation", error)
        finally:
            cursor.execute("UPDATE students SET class_id = (%s) WHERE first_name = (%s );", (class_id, first_name))


# """ALTER TABLE students
#    ADD CONSTRAINT students_class_id_fkey
#    FOREIGN KEY(class_id) REFERENCES class_room(class_id)
#    ON DELETE CASCADE ON UPDATE CASCADE;"""


def delete_student_record():
    try:
        students = get_students(12)
        print("student(s) ", students)
        first_name = input("Enter the first name of student to be removed from a class: ")
        student_id = int(input("Enter student's id: "))
        with CursorFromConnectionPool() as cursor:
            cursor.execute("DELETE FROM students WHERE first_name = %s AND student_id = %s;", (first_name, student_id,))
            count = cursor.rowcount
            print(count, "record deleted successfully")

    except (Exception, psycopg2.Error) as error:
        print("Error in operation", error)


menu()
