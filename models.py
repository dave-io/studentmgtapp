from database import CursorFromConnectionPool


class ClassModel:
    class_name = None

    def __init__(self, class_name, class_size, class_floor, teacher, age_bracket, id=None):
        self.class_name = class_name
        self.class_size = class_size
        self.class_floor = class_floor
        self.teacher = teacher
        self.age_bracket = age_bracket
        self.id = id

    def save_class_to_db(self):
        with CursorFromConnectionPool() as cursor:
            cursor.execute('INSERT INTO class_room (class_name, class_size, class_floor, teacher, age_bracket) '
                           'VALUES (%s, %s, %s, %s, %s);',
                           (self.class_name, self.class_size, self.class_floor, self.teacher, self.age_bracket))

    def update_class_to_db(self):
        with CursorFromConnectionPool() as cursor:
            sql = """UPDATE class_room SET class_name = (%s), class_size = (%s), class_floor = (%s),
            teacher = (%s), age_bracket = (%s)
            WHERE class_name = (%s)"""

            cursor.execute(sql, (self.class_name, self.class_size, self.class_floor, self.teacher, self.age_bracket,
                                 self.class_name,))

            return cursor.rowcount


class Students(ClassModel):
    def __init__(self, first_name, last_name, age, gender, dob, reg_date, phone, class_id):
        # super().__init__(self, class_name, class_size, class_floor, teacher, age_bracket)
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.gender = gender
        self.dob = dob
        self.reg_date = reg_date
        self.phone = phone
        self.class_id = class_id

    def save_student_to_db(self):
        with CursorFromConnectionPool() as cursor:
            cursor.execute('INSERT INTO students (first_name, last_name, age, gender, dob, reg_date, phone, class_id)'
                           'VALUES (%s, %s, %s, %s, %s, %s, %s, %s);',
                           (self.first_name, self.last_name, self.age, self.gender,
                            self.dob, self.reg_date, self.phone, self.class_id))

    def update_student_to_db(self):
        with CursorFromConnectionPool() as cursor:
            sql = """UPDATE students SET first_name = (%s), last_name = (%s), age = (%s),
            gender = (%s), dob = (%s), reg_date = (%s), phone = (%s), class_id = (%s)
            WHERE first_name = (%s) AND last_name = (%s)"""

            cursor.execute(sql, (self.first_name, self.last_name, self.age, self.gender,
                                 self.dob, self.reg_date, self.phone, self.class_id, self.first_name, self.last_name))

            # sql = "UPDATE students SET first_name = {0}, last_name = {1}, age = {2}, \
            # gender = {3}, dob = {4}, reg_date = {5}, phone = {6}, class_id = {7} \
            # WHERE first_name = {0} AND last_name = {1}".format(self.first_name, self.last_name, self.age, self.gender,
            #                                                    self.dob, self.reg_date, self.phone, self.class_id)
            #
            # cursor.execute(sql,)
            return cursor.rowcount
