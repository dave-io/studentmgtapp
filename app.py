# Write a programme where the user can create classes and then add students to the class.
# The programme connects to a database to add the class and add the students.
# The user should be able to create, update and delete a student record.
# The user should be able to update a class information.
# The data captured for the class includes; class name, class size, class floor (a imaginary building with several floors), class teachers full name, class age bracket.
# The data capture for students are firstname, lastname, age, gender, date of birth, date of registration and parents phone number.


import psycopg2
from models import ClassModel
from models import Students
import student_registration


def menu():
    print("Welcome to your student management application!")
    print("Press 1 to manage class")
    print("Press 2 to manage students")
    option = int(input("***************************\n"))
    if option == 1:
        manage_class()
    elif option == 2:
        manage_students()


def manage_class():
    print("Press 1 to CREATE class")
    print("Press 2 to UPDATE class")
    option = int(input("******************************\n"))
    if option == 1:
        create_class()
    elif option == 2:
        update_class()


def manage_students():
    print("Press 1 to ADD a student to a class")
    print("Press 2 to UPDATE student's record")
    print("Press 3 to DELETE student's record")
    option = int(input("******************************\n"))
    if option == 1:
        add_student()
    elif option == 2:
        update_student_record()
    elif option == 3:
        delete_student_record()


menu()

